#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE DATABASE whatscooking;
EOSQL
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" whatscooking <<-EOSQL
    CREATE EXTENSION HSTORE;
EOSQL
