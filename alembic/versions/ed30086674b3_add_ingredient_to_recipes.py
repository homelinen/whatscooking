"""Add Ingredient to Recipes

Revision ID: ed30086674b3
Revises: 0571fbc10f41
Create Date: 2016-09-25 18:55:34.711094

"""

# revision identifiers, used by Alembic.
revision = 'ed30086674b3'
down_revision = '0571fbc10f41'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.create_table(
        'recipes_ingredients',
        sa.Column('recipe_id', sa.Integer),
        sa.Column('ingredient_id', sa.Integer),
    )

    op.create_foreign_key(
        'fk_recipe_ingredients',
        'recipes_ingredients',
        'recipes',
        ['recipe_id'],
        ['id'],
    )
    op.create_foreign_key(
        'fk_ingredient_recipes',
        'recipes_ingredients',
        'ingredients',
        ['ingredient_id'],
        ['id'],
    )

    op.create_primary_key('pk_recipe_ingredient', 'recipes_ingredients',
                          ['recipe_id', 'ingredient_id'])


def downgrade():
    op.drop_table('recipes_ingredients')
