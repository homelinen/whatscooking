"""Create Users Table

Revision ID: 42b1cafc41bf
Revises: ed30086674b3
Create Date: 2017-01-08 23:15:56.925476

"""

# revision identifiers, used by Alembic.
revision = '42b1cafc41bf'
down_revision = 'ed30086674b3'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(32), nullable=False),
        sa.Column('password', sa.Binary(60), nullable=False),
        sa.Column('is_active', sa.Boolean, nullable=False),
    )


def downgrade():
    op.drop_table('users')
