"""Add Ingredients Table

Revision ID: 0571fbc10f41
Revises: 16d3479022dd
Create Date: 2016-09-25 18:47:01.963372

"""

# revision identifiers, used by Alembic.
revision = '0571fbc10f41'
down_revision = '16d3479022dd'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'ingredients',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(80), nullable=False),
    )


def downgrade():
    op.drop_table('recipes')

