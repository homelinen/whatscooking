# What's Cooking

Manage your weeks meal.

## Developing

Requires a Postgres DB with the HSTORE extension. 

### Setup

```
CREATE DATABASE whatscooking;
\c whatscooking
CREATE EXTENSION HSTORE;
```
