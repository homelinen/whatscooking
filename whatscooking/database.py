from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Database:
    def __init__(self, database_password):
        psql_connection='postgresql+psycopg2://postgres:{password}@127.0.0.1/whatscooking'.format(
            password=database_password
        )
        engine = create_engine(psql_connection, convert_unicode=True)
        self.db_session = scoped_session(sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=engine
        ))

        # FIXME: This is a bad idea to link a class property in another class
        Base.query = self.db_session.query_property()

        # import all modules here that might define models so that
        # they will be registered properly on the metadata.  Otherwise
        # you will have to import them first before calling init_db()
        import whatscooking.models
        Base.metadata.create_all(bind=engine)
