from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict

from whatscooking.database import Base
from whatscooking.models.ingredients import Ingredient


class RecipeNotFoundException(Exception):
    pass


class Recipe(Base):
    __tablename__ = 'recipes'

    id = Column(Integer, primary_key=True)
    name = Column(String(80), nullable=False)
    ingredients = Column(MutableDict.as_mutable(HSTORE), nullable=False)

    def __init__(self, name, ingredients={}):
        self.name = name
        self.ingredients = ingredients

    def set_ingredient_quantity(self, ingredient_id, quantity):
        """
        Add an ingredient to the internal ingredient/quantity hash

        quantity can be any arbitrary value and unit. 3 cups, 2kg, 2 fist sized, etc
        """

        ingredients[ingredient_id] = {
            'name': Ingredient.query.get(ingredient_id).name,
            'quantity': quantity,
        }

