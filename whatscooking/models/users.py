from sqlalchemy import Column, Integer, String, Binary, Boolean

from whatscooking.database import Base

class UserNotFoundException(Exception):
    pass


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    password = Column(Binary(60), nullable=False)
    is_active = Column(Boolean, nullable=False)
    is_authenticed = False

    def __init__(self, name, password, is_active=False):
        self.name = name
        self.password = password
        self.is_active = False

    def get_id(self):
        """
        ID implementation for Flask-Login
        """
        return self.id

