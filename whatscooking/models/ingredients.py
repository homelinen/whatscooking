from sqlalchemy import Column, Integer, String, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import HSTORE

from whatscooking.database import Base

class IngredientNotFoundException(Exception):
    pass

class Ingredient(Base):
    __tablename__ = 'ingredients'

    id = Column(Integer, primary_key=True)
    name = Column(String(80), nullable=False)

    recipes = relationship(
        "Recipe",
        secondary=Table(
            'recipes_ingredients',
            Base.metadata,
            Column('recipe_id', Integer, ForeignKey('recipes.id'),
                   primary_key=True),
            Column('ingredient_id', Integer, ForeignKey('ingredients.id'),
                   primary_key=True),
        ),
        backref='ingredients_rel',
    )

    def __init__(self, name, ingredients={}):
        self.name = name
        self.ingredients = ingredients
