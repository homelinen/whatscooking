Flask==0.10.1
Flask-Assets==0.11
Flask-Login==0.4.0
Flask-WTF==0.14
alembic==0.8.5
SQLAlchemy==1.0.12
psycopg2==2.6.1

bcrypt==3.1.2
itsdangerous==0.24
