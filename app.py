import itertools
import logging
import os
import json

from flask import (
    Flask,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask.ext import assets

from flask_login import (
    LoginManager, login_user, logout_user, login_required)
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired

import bcrypt

from whatscooking.forms import RedirectForm
from whatscooking.database import Database
from whatscooking.models.ingredients import Ingredient
from whatscooking.models.recipes import Recipe
from whatscooking.models.users import User
from whatscooking.fixtures import recipe_list, week_list

from whatscooking.sessions import ItsdangerousSessionInterface


logging.basicConfig()
logger = logging.getLogger('sqlalchemy.engine')
logger.setLevel(logging.DEBUG)

logger = logging.getLogger('root')

app = Flask(__name__)
env = assets.Environment(app)

# Tell flask-assets where to look
env.load_path = [
    os.path.join(os.path.dirname(__file__), 'bower_components'),
    "assets",
]

env.register(
    'css_all',
    assets.Bundle(
        'normalize-css/normalize.css',
        'Pure Extras/css/pure-extras.css',
        'pure/src/base/css/base.css',
        'pure/src/tables/css/tables.css',
        'pure/src/grids/css/grids-core.css',
        'pure/src/buttons/css/buttons.css',
        'pure/src/forms/css/forms.css',
        "main.css",
        output='all_css.css'
    )
)

# Flask-Login
login_manager = LoginManager()

@app.route('/')
def home():
    return render_template('home.html.jinja2')


@app.route('/recipes')
def recipes():
    # FIXME: Stop copying me
    recipe_list = Recipe.query.all()
    return render_template('recipes.html.jinja2', recipes=recipe_list)

@app.route('/ingredients')
def all_ingredients():
    ingredients_list = Ingredient.query.all()
    return render_template('ingredients.html.jinja2', ingredients=ingredients_list)


@app.route('/ingredient/create')
@login_required
def create_ingredient():
    return render_template('ingredient_create.html.jinja2')


# FIXME: Use IDs for ingredients
@app.route('/ingredients/<ingredient_id>')
def get_ingredient(ingredient_id):
    db_session = app.config["database"].db_session
    ingredient = Ingredient.query.filter(Ingredient.name==ingredient_id)\
                                 .first()

    return render_template(
        'ingredient.html.jinja2',
        name=ingredient.name,
        recipes=ingredient.recipes,
    )


# FIXME: Call this submit_recipe
@app.route('/ingredients', methods=['POST'])
@login_required
def submit_ingredient():

    # FIXME: Sanitize this
    name = request.form['name']
    i = Ingredient(name)

    db_session = app.config["database"].db_session
    # FIXME: Names don't have to be unique
    db_session.add(i)
    db_session.commit()

    return redirect(url_for('get_ingredient', ingredient_id=name))


# FIXME: Use IDs for recipes
@app.route('/recipes/<recipe_id>')
def get_recipe(recipe_id):
    db_session = app.config["database"].db_session
    recipe = Recipe.query.filter(Recipe.name==recipe_id)\
                         .first()

    if not recipe:
        abort(404)

    return render_template(
        'recipe.html.jinja2',
        name=recipe.name,
        recipe_id = recipe.id,
        ingredients=recipe.ingredients_rel,
        ingredients_extras=recipe.ingredients,
        # instructions=recipe.steps,
    )


@app.route('/recipes/<recipe_id>/edit')
@login_required
def edit_recipe(recipe_id):
    db_session = app.config["database"].db_session
    recipe = Recipe.query.get(recipe_id)

    if not recipe:
        abort(404)

    return render_template('recipe_update_ingredients.html.jinja2',
                           recipe_id=recipe.id,
                           name=recipe.name,
                           ingredients=recipe.ingredients_rel,
                           ingredients_extras=recipe.ingredients,
                           )

    

@app.route('/recipes/<recipe_id>/ingredients/<ingredient_id>', methods=['POST'])
@login_required
def update_recipe_ingredient(recipe_id, ingredient_id):
    db_session = app.config["database"].db_session
    recipe = Recipe.query.get(recipe_id)
    ingredient = Ingredient.query.get(ingredient_id)
    if not recipe or not ingredient:
        abort(404)

    # TODO: Turn ingredients field set into JSON
    quantity = request.form["quantity"]

    recipe.ingredients["{}".format(ingredient.id)] = quantity.strip()

    db_session.merge(recipe)
    db_session.commit()

    return render_template(
        'recipe.html.jinja2',
        name=recipe.name,
        ingredients=recipe.ingredients_rel,
        ingredients_extras=recipe.ingredients,
        # instructions=recipe.steps,
    )


@app.route('/recipe/create')
@login_required
def create_recipe_page():
    return render_template('recipe_create.html.jinja2',
                           name="",
                           ingredients=[],
                           )


# FIXME: Call this submit_recipe
@app.route('/recipes', methods=['POST'])
@login_required
def submit_recipe():

    # FIXME: Sanitize this
    name = request.form['name']
    ingredients_form = request.form['ingredients']

    error = ""
    if name.strip() == '':
        error = "Name cannot be empty"
    if ingredients_form.strip() == '':
        error = "ingredients cannot be empty"

    if error:
        flash(error)
        redirect(url_for('create_recipe_page')) 

    ingredients = split_ingredients(ingredients_form)
    if ingredients == None:
        error = "Ingredients should be separated by ','s or on new lines" 
        flash(error)
        redirect(url_for('create_recipe_page')) 


    db_session = app.config["database"].db_session
    # TODO: Use the ingredient HSTORE for recipes
    r = Recipe(name)

    db_session.add(r)

    for ing in ingredients:
        ingredient = Ingredient(ing)
        ingredient.recipes = [r]

        db_session.add(ingredient)

    db_session.commit()


    return redirect(url_for('get_recipe', recipe_id=name))


def split_ingredients(ingredients_list):
    if ',' in ingredients_list:
        ingredients_split = [ i.strip() for i in ingredients_list.split(',') ]
    elif '\n' in ingredients_list:
        ingredients_split = [ i.strip() for i in ingredients_list.split('\n') ]
    else:
        ingredients_split = []

    ingredients = list(filter(bool, ingredients_split))

    return ingredients


def get_recipe_ingredients(recipe):
    """
    Lookup the ingredients belonging to a recipe
    """

    # FIXME: Stop copying me
    recipe_list = Recipe.query.all()

    ingredients = list(itertools.chain.from_iterable([
        i['ingredients']
        for i in recipe_list
        if i.name == recipe
    ]))

    return ingredients


@app.route('/thisweek')
@login_required
def get_week():
    # FIXME: Assummed Sorted
    week = week_list[list(week_list.keys())[-1]]

    week_view = []

    for day, recipes in week.items():
        v_item = {}
        v_item['day'] = day

        # Turn recipes into links?
        v_item['recipes'] = recipes

        # get ingredients from recipe
        v_item['ingredients'] = []
        for r in recipes:
            v_item['ingredients'] = (
                v_item['ingredients'] + get_recipe_ingredients(r))

        week_view.append(v_item)

    days = [day['day'] for day in week_view]
    return render_template('week_view.html.jinja2',
                           week_view=week_view, days=days)

class LoginForm(RedirectForm):
    name = StringField('name', validators=[
        #DataRequired()
        ])
    password = PasswordField('password', validators=[
        #DataRequired()
        ])


class SignupForm(RedirectForm):
    name = StringField('name', validators=[
        #DataRequired()
        ])
    password = PasswordField('password', validators=[
        #DataRequired()
        ])

    def __init__(self):
        super(SignupForm, self).__init__()
        self.next.data = 'home'


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.is_submitted():
        user = User(
            name=form.data['name'],
            password=bcrypt.hashpw(form.data['password'].encode('utf-8'), bcrypt.gensalt())
            )

        # FIXME: Check if user already exists
        db_session = app.config["database"].db_session
        db_session.add(user)
        db_session.commit()
        return form.redirect(url_for('home'))

    return render_template('signup.html.jinja2', form=form)



@app.route('/login', methods=['GET', 'POST'])
def login():
    # FIXME: Don't render the page if already logged in
    form = LoginForm()

    # if form.validate_on_submit():
    if form.is_submitted():
        db_session = app.config["database"].db_session
        user = User.query.filter(User.name == form.data['name'])\
                                 .first()

        if user == None:
            flash('Username or password incorrect')
            return render_template('login.html.jinja2', form=form)

        check = bcrypt.checkpw(form.data['password'].encode('utf-8'), user.password)

        if not check:
            flash('Username or password incorrect')
            return render_template('login.html.jinja2', form=form)

        user.is_authenticated = True
        login_user(user)

        flash('Logged in successfully.')

        return form.redirect('home')

    return render_template('login.html.jinja2', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


@login_manager.user_loader
def load_user(user_id):
    user = User.query.get(user_id)
    if user:
        user.is_authenticated = True
    return user


@app.teardown_appcontext
def shutdown_session(exception=None):
    app.config["database"].db_session.remove()


if __name__ == "__main__":
    database = Database(database_password=os.environ["POSTGRES_PASSWORD"])
    app.config["database"] = database

    app.secret_key = 'SuperblySecret'
    app.session_interface = ItsdangerousSessionInterface()

    login_manager.login_view = 'login'
    login_manager.init_app(app)
    app.debug = True
    app.run()
